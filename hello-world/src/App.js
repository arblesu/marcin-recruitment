import React from "react";
import "./style.css";

function App() {
  return (
    <div>
      <form>
        <input placeholder="Item name..." />
        <button>Add new item</button>
      </form>

      <ul>
        <li>Item 1</li>
        <li>Item 2</li>
      </ul>
    </div>
  );
}

export default App;
