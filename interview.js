/**
 *
 *  A = [1,2,3,4,5]  B = [1,2,3,4,5] => [2,4,6,8,10]
 *  1. Creates a new cloned array
 *  2. Double the values in new array
 *  3. return the new array
 */

function cloneAndDouble(arr) {
  //Code here
}

// const input = [1, 2, 3, 4, 5];
// const doubled = cloneAndDouble(input);
// console.log("Original", input);
// console.log("Doubled: ", doubled);

/**
 *
 *  Input => [10,20,30,40,50]  Output => [10, 30, 40, 50]
 *  1. Remove a value at index 'i' and return the new array without modifiying original array.
 */

function removeAtIndex(arr, index) {
  //Code here
}

// const input = [10, 20, 30, 40, 50];
// const indexToRemove = 1;
// const removed = removeAtIndex(input);
// console.log("Original", input);
// console.log("Removed: ", removed);

/*
 * Input: List of items (products) which contains a category.
 * Return the list of items of a specific category.
 *
Input: [
  {
    category: "Water",
    name: "Coca-cola",
  },
  {
    category: "Food",
    name: "Pizza",
  },
  {
    category: "Water",
    name: "Pepsi",
  },
]

Output: [
  {
    category: "Water",
    name: "Coca-cola",
  },
  {
    category: "Water",
    name: "Pepsi",
  },
]
 */

function filterItems(itemsList, category) {
  //Code here
}

// const list = [
// {
// category: "Water",
// name: "Coca-cola",
// },
// {
// category: "Food",
// name: "Pizza",
// },
// {
// category: "Water",
// name: "Pepsi",
// },
// ];
// const filteredItems = filterItems(list, "Water");
// console.log("Original", list);
// console.log("Output", filteredItems);
